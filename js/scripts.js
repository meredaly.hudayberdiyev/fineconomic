//?<IBG>===================================================================================================================================================================================================
function ibg() {
    $.each($('.ibg'), function (index, val) {
        if ($(this).find('img').length > 0) {
            $(this).css('background-image', 'url("' + $(this).find('img').attr('src') + '")');
        }
    });
}
ibg();
//?</IBG>==================================================================================================================================================================================================

//?<NAVBAR [BURGER]>
$(document).ready(function () {
    $('.icon-menu').click(function (event) {
        $('.icon-menu, .menu').toggleClass('active');
        $('body').toggleClass('_lock');
    });
});
//?</NAVBAR [BURGER]>



// $(document).ready(function () {
//     $('.currency-slider').slick({
//         dots: false,
//         slidesToShow: 7,
//         slidesToScroll: 1,
//         autoplay: true,
//         autoplaySpeed: 0,
//         speed: 3000,
//         arrows: false,
//         pauseOnFocus: false,
//         pauseOnHover: false,
//         pauseOnDotsHover: false,
//         swipe: false,
//         cssEase: 'linear',
//     });
// });
$(document).ready(function ($) {
    $('.currency-slider').show();

    $('.currency-slider').marquee({
        speed: 30,
        gap: 0,
        delayBeforeStart: 0,
        direction: 'left',
        duplicated: true,
        pauseOnHover: false,
        startVisible: true
    });
});

$(document).ready(function ($) {
    $('.mini-tender__slider').show();

    $('.mini-tender__slider').marquee({
        speed: 30,
        gap: 0,
        delayBeforeStart: 0,
        direction: 'left',
        duplicated: true,
        pauseOnHover: true,
        startVisible: true
    });
});



$(document).ready(function () {
    $('.magazine-slider').slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 3000,
        arrows: false,
        pauseOnFocus: false,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        swipe: true,
    });
});

$(document).ready(function () {
    $('.big-slider').slick({
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 3000,
        arrows: false,
        pauseOnFocus: false,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        swipe: true
    });
});



$(document).ready(function () {
    $('.purpose__row').slick({
        dots: false,
        // slidesToShow: 4,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 1000,
        arrows: false,
        pauseOnFocus: true,
        pauseOnHover: true,
        pauseOnDotsHover: false,
        swipe: true,
        variableWidth: true,
    });
});
$(document).ready(function () {
    $('.main-gallery__slider').slick({
        dots: false,
        slidesToShow: 4,
        autoplay: false,
        autoplaySpeed: 2000,
        speed: 1000,
        arrows: true,
        pauseOnFocus: true,
        pauseOnHover: true,
        pauseOnDotsHover: false,
        swipe: true,
    });
});


$(document).ready(function () {
    $('.section__title').click(function (event) {
        if ($('.section').hasClass('one')) {
            $('.section__title').not($(this)).removeClass('active');
            $('.section__subsection').not($(this).next()).slideUp(300);
        }
        $(this).toggleClass('active').next().slideToggle(300);
    })
});

$(document).ready(function () {
    $('.factory-form__title-mini').click(function (event) {
        $(this).toggleClass('active').next().slideToggle(300);
    })
});


function sort_c1() {
    let checkbox = document.getElementById("sort_c1").checked;
    if (checkbox == true) {
        document.getElementById("sort_s1").classList.toggle("active");
    }
    else {
        document.getElementById("sort_s1").classList.toggle("active");
    }
}

function sort_c2() {
    let checkbox = document.getElementById("sort_c2").checked;
    if (checkbox == true) {
        document.getElementById("sort_s2").classList.toggle("active");
    }
    else {
        document.getElementById("sort_s2").classList.toggle("active");
    }
}


//? <Header>================================================================================================================
$(window).on('scroll', function () {
    if ($(window).scrollTop()) {
        $('.header').addClass('fixed');
        $('.page').addClass('fixed');
    }
    else {
        $('.header').removeClass('fixed');
        $('.page').removeClass('fixed');
    }
    // console.log($(window).scrollTop());
    let topScr = 0;
    let addcls = '';
    if ($('.big-slider-container').length > 0) {
        let add = 76;
        let sc = document.getElementsByClassName('big-slider-container')[0];
        if (sc.offsetWidth < 1250) {
            add = 50;
        }
        if (sc.offsetWidth < 500) {
            add = 0;
        }
        topScr = sc.scrollHeight;
        addcls = 'menu-nh';
    }

    if ($(window).scrollTop() > topScr) {
        $('.menu').addClass('fixed ' + addcls);
        $('.mini-tender').addClass('fixed');
    }
    else {
        $('.menu').removeClass('fixed ' + addcls);
        $('.mini-tender').removeClass('fixed');
    }
    // if (document.getElementsByClassName('in-news-ha').length === 1) {
    //     $('.header__back').addClass('active');
    //     console.log('true');
    // }
});
// sidebar slider
$(document).ready(function () {
    $('.slogan__row').slick({
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 3000,
        arrows: false,
        pauseOnFocus: true,
        pauseOnHover: true,
        pauseOnDotsHover: true,
        swipe: true,
        variableWidth: true,
    });
    $('.sidebar-news__row').slick({
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 3000,
        arrows: false,
        pauseOnFocus: true,
        pauseOnHover: true,
        pauseOnDotsHover: true,
        swipe: false,
        variableWidth: true,
    });
});
// ? </Header>================================================================================

function openDoc(index) {
    let classL = document.getElementsByClassName('form-wc__download-file--form');
    document.getElementById('doc-opened').style.display = "block";
    for (let i = 0; i < classL.length; i++) {
        classL[i].classList.remove('active');
    }
    // document.getElementById('embed').src = "http://docs.google.com/viewer?url=" + document.getElementById('doc-' + index).href + "&embedded=true";
    document.getElementById('embed').src = document.getElementById('doc-' + index).href;
    // document.getElementById('def-t').innerText = document.getElementById('title-' + index).innerText;
    classL[index].classList.add('active');
}

function openSearch() {
    document.getElementById("myOverlay").style.display = "block";
    document.getElementsByClassName("header")[0].classList.add("index-top");
}

function closeSearch() {
    document.getElementById("myOverlay").style.display = "none";
    document.getElementsByClassName("header")[0].classList.remove("index-top");
}



$(document).ready(function () {
    $('.home-sections__title').click(function (event) {
        if ($('.home-sections__row').hasClass('one')) {
            $('.home-sections__title').not($(this)).removeClass('active');
            $('.home-sections__subsection').not($(this).next()).slideUp(300);
        }


        console.log($(this)[0].id);
        let a = $(this)[0].id;

        $("svg#map > *").removeClass('on');
        $('.loc').removeClass('active');

        if (a == 'ashgabat') {
            a = 'ahal';
        }
        $('#map-' + a).toggleClass('on');
        $('.loc-' + a).toggleClass('active');

        $(this).toggleClass('active').next().slideToggle(300);
    })



    let region = $("svg#map > *");
    region.on("click", function (event) {
        if ($('.home-sections__row').hasClass('one')) {
            $('.home-sections__title').not($(this)).removeClass('active');
            $('.home-sections__subsection').not($(this).next()).slideUp(300);
        }

        region.removeClass("on");
        let a = $(this)[0].id;
        a = a.slice(4, a.length);
        console.log(a);
        $('.loc').removeClass('active');

        $('#map-' + a).toggleClass('on');
        $('.loc-' + a).toggleClass('active');
        $('#' + a + '.home-sections__title').toggleClass('active').next().slideToggle(300);
    });

});

$(document).ready(function () {
    $('.form-news__category--main-title').click(function (event) {
        $(this).toggleClass('active');
        $('.form-news__category--row').toggleClass('active');
    })
});

$(document).ready(function () {
    $('.category__title').click(function (event) {
        $(this).toggleClass('active');
        $('.category__row').toggleClass('active');
    })
});
$(document).ready(function () {
    $('.other-news__title').click(function (event) {
        $(this).toggleClass('active');
        $('.other-news__row').toggleClass('active');
    })
});