// const input[type=file] id="file"
// const id="file"->fileList
const fileSelect = document.getElementById("form_file"),
	fileList = document.getElementById("fileList");

// add fileList elements to HTML
function fileListAdd(files) {
	let addList = "";
	if (files.length > 0) {
		document.getElementById('send-docs__after').style.display = "block";
		document.getElementById('send-docs__before').style.display = "none";
	}
	else {
		document.getElementById('send-docs__after').style.display = "none";
		document.getElementById('send-docs__before').style.display = "block";
	}
	for (let i = 0; i < files.length; i++) {
		// addList = addList + '<div id="file-' + i + '"><div class="name">' + files[i].name + '</div><button class="remove" onclick="remove(' + i + ')">Remove</button></div>';
		addList = addList + '<div class="send-docs__dow-f-col">' +
			'<div class="send-docs__dow-f-type" > <img src="img/ico/PDF.svg"></div>' +
			'<div class="send-docs__dow-f-title ">' + files[i].name + '</div>' +
			'<div class="send-docs__dow-f-load dis">' +
			'<div class="download" style="width: 0%;"></div>' +
			'</div>' +
			'<button class="send-docs__dow-f-remove" onclick="remove(' + i + ')"><img src="img/ico/remove.svg"></button>' +
			'</div>';
	}
	let toDOM = document.createElement('div');
	toDOM.innerHTML = addList;
	return toDOM;
}

function remove(id) {
	console.log(id);
	let newElem = new ClipboardEvent("").clipboardData || new DataTransfer();
	for (let i = 0; i < fileSelect.files.length; i++) {
		if (i !== id) {
			newElem.items.add(fileSelect.files[i]);
		}
	}
	fileSelect.files = newElem.files;
	update();
}

function removeAll() {
	fileList.innerHTML = '';
}

// clear last elements and add new
function update() {
	removeAll();
	let files = fileSelect.files;
	fileList.appendChild(fileListAdd(files));
}

fileSelect.addEventListener("change", function (e) {
	update();
}, false);

